#!/bin/bash

python manage.py collectstatic --noinput
python manage.py makemigrations
python manage.py migrate
gunicorn project_api.wsgi:application -w 2 -b :9001 --timeout 300
