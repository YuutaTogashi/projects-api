from rest_framework import serializers
from projects.models import Project, Member
from rest_framework_serializer_extensions.fields import HashIdField, HashIdHyperlinkedRelatedField


class ProjectSerializer(serializers.ModelSerializer):
    id = HashIdField(model=Project, required=False)
    class Meta:
        model = Project
        fields = ('title',
                  'description',
                  'area',
                  'owner',
                  'photo',
                  'id')



class MemberSerializer(serializers.ModelSerializer):
    project = serializers.ReadOnlyField(source='project.id')
    id = HashIdField(model=Member, required=False)
    class Meta:
        model = Member
        fields = ('title',
                  'description',
                  'skills',
                  'area',
                  'project',
                  'id')