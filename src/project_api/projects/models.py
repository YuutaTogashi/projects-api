from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.conf import settings


class Project(models.Model):
    title = models.CharField(max_length=256)
    owner = models.CharField(max_length=256)
    description = models.TextField()
    area = models.CharField(max_length=256)
    photo = models.TextField(default='')


class Member(models.Model):
    title = models.CharField(max_length=256)
    area = models.CharField(max_length=256)
    description = models.TextField()
    skills = ArrayField(models.CharField(max_length=128))
    user = models.CharField(max_length=256, default=None, null=True)
    project = models.ForeignKey('projects.Project', related_name='members', on_delete=models.CASCADE)
