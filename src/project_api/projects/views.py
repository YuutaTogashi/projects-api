from django.shortcuts import render
from projects.models import Project, Member
from projects.serializers import ProjectSerializer, MemberSerializer
from projects.permissions import IsAuth, CsrfExemptSessionAuthentication, BasicAuthentication, get_owner
from projects.word_extractor import catch
from projects import HASH_IDS

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import os
from django.conf import settings
from django.http import Http404

from datetime import datetime
from elasticsearch import Elasticsearch, ElasticsearchException


search_rules = {
    'info': lambda mess: (' '.join(catch(mess)), 'should', 1),
    'city': lambda city: (city, 'must',1),
    'area': lambda area: (area, 'must', 1),
    'skills_info': lambda skills: (' '.join(catch(skills)), 'should', 2)
}

def get_es():
    if 'ELASTIC_HOST' in os.environ:
        return Elasticsearch([os.environ['ELASTIC_HOST']])
    else:
        return Elasticsearch()

def hash_decode(hash):
    return HASH_IDS.decode(hash)[1]

def save_to_elastic(member, project, city):
    skills_info = catch(' '.join(member.skills).lower())
    info = skills_info + catch(member.title.lower())
    es = get_es()
    doc = {
        'project_id': project.id,
        'project_title': project.title,
        'title': member.title,
        'info': info,
        'skills': member.skills,
        'skills_info': skills_info,
        'timestamp': datetime.now(),
        'city': city
    }
    es.index(index="members", id=member.id, body=doc, doc_type="member")

def delete_from_elastic(id):
    es = get_es()
    es.delete(index="members", id=id, doc_type="member")


class ProjectBasicView(APIView):
    def get_objects(self, owner):
        try:
            return Project.objects.filter(owner=owner)
        except Project.DoesNotExist:
            raise Http404

    def get_object(self, id):
        try:
            return Project.objects.get(id=id)
        except Project.DoesNotExist:
            raise Http404


class MemberBasicView(APIView):
    def get_objects(self, project_id):
        try:
            return Member.objects.filter(project=project_id)
        except Member.DoesNotExist:
            raise Http404

    def get_object(self, id):
        try:
            return Member.objects.get(id=id)
        except Member.DoesNotExist:
            raise Http404

    def get_project(self, owner, project_id):
        try:
            project = Project.objects.get(id=project_id)
            if project.owner != owner:
                return None
            else:
                return project
        except Project.DoesNotExist:
            raise Http404

    def get_owner(self, member_id):
        try:
            member = Member.objects.get(id=member_id)
            project = member.project
            return project.owner
        except Member.DoesNotExist:
            raise Http404



class ProjectList(ProjectBasicView):
    permission_classes = (IsAuth,)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, format=None):
        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        projects = self.get_objects(username)
        serializer = ProjectSerializer(projects, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        user =  get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        data = request.data
        data.pop('id', None)
        data['owner'] = username
        serializer = ProjectSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, format=None):
        if 'id' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        project_id = hash_decode(request.data['id'])
        project= self.get_object(project_id)
        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        if username != project.owner:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        data = request.data
        data.pop('id', None)
        data['owner'] = username
        serializer = ProjectSerializer(project, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        if 'id' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        project_id = hash_decode(request.data['id'])
        project = self.get_object(project_id)
        if username != project.owner:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        project.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class ProjectDetail(ProjectBasicView):
    def get(self, request, project_hash, format=None):
        project_id = hash_decode(project_hash)
        project = self.get_object(project_id)
        serializer = ProjectSerializer(project)
        return Response(serializer.data)


class MemberList(MemberBasicView):
    permission_classes = (IsAuth,)
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def get(self, request, project_hash, format=None):
        project_id = hash_decode(project_hash)
        members = self.get_objects(project_id)
        serializer = MemberSerializer(members, many=True)
        return Response(serializer.data)

    def post(self, request, project_hash, format=None):
        project_id = hash_decode(project_hash)

        if settings.AUTH_HEADER not in request.META:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        project_instance = self.get_project(username, project_id)
        if not project_instance:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        data = request.data
        data.pop('id', None)

        serializer = MemberSerializer(data=request.data)
        if serializer.is_valid():
            member = serializer.save(project=project_instance)
            save_to_elastic(member, project_instance, user['city'])
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MemberDetail(MemberBasicView):
    def get(self, request, member_hash, format=None):
        member_id = hash_decode(member_hash)
        member = self.get_object(member_id)
        serializer = MemberSerializer(member)
        return Response(serializer.data)

    def delete(self, request, member_hash, format=None):
        member_id = hash_decode(member_hash)
        if settings.AUTH_HEADER not in request.META:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        if self.get_owner(member_id) != username:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        resume = self.get_object(member_id)
        resume.delete()
        delete_from_elastic(member_id)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, member_hash, format=None):
        member_id = hash_decode(member_hash)
        if settings.AUTH_HEADER not in request.META:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        user = get_owner(request)
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        username = user[settings.OWNER_FIELD]

        member = self.get_object(member_id)
        project_instance = self.get_project(username, member.project.id)
        if not project_instance:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        member = self.get_object(member_id)
        data = request.data
        data.pop('project', None)
        data.pop('id', None)

        serializer = MemberSerializer(member, data=data)
        if serializer.is_valid():
            member = serializer.save()
            save_to_elastic(member, project_instance, user['city'])
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SearchMember(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    def post(self, request, format=None):
        template = {
            'query': {
                'bool': {
                    'must': [{
                        'bool': {
                            'should': []}
                    }]
                }
            }
        }

        data = {'must': [],
                'should': []
                }
        if 'from' not in request.data and 'size' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        for rule in search_rules:
            if rule not in request.data:
                continue
            tmp = {'match': {}}
            rules = search_rules[rule](request.data[rule])
            tmp['match'][rule] = {}
            tmp['match'][rule]['query'] = rules[0]
            tmp['match'][rule]['boost'] = rules[2]
            data[rules[1]].append(tmp)
            # data['query']['match'][rule] = search_rules[rule](request.data[rule])
        template['query']['bool']['must'][0]['bool']['should'] = data['should']
        template['query']['bool']['must'] += data['must']

        if len(data['should']) == 0:
            template['query'] = {'match_all': {}}

        es = get_es()
        try:
            hits = es.search(index='members',
                             doc_type='member',
                             body=template,
                             from_=request.data['from'],
                             size=request.data['size'])['hits']['hits']
        except ElasticsearchException:
            return Response(status=status.HTTP_404_NOT_FOUND)

        res = []
        for hit in hits:
            tmp = hit['_source']
            tmp['id'] = hit['_id']
            tmp.pop('info', None)
            tmp.pop('timestamp', None)
            tmp.pop('skills_info', None)
            try:
                member = Member.objects.get(id=tmp['id'])
                project =  ProjectSerializer(member.project)
                member = MemberSerializer(member)
                tmp['id'] = member.data['id']
                tmp['project_id'] = project.data['id']
            except Member.DoesNotExist:
                es.delete(index='members', doc_type='member', id=tmp['id'])
                continue
            except Project.DoesNotExist:
                es.delete(index='members', doc_type='member', id=tmp['id'])
                continue
            res.append(tmp)

        return Response(res)











