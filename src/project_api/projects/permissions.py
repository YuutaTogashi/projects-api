import requests
from rest_framework import permissions
from django.conf import settings
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

def get_owner(request):
    if settings.AUTH_HEADER not in request.META:
        return None

    header = {'Authorization': request.META[settings.AUTH_HEADER]}
    r = requests.get(settings.IDENTITY_PROVIDER, headers=header)
    identity_response = r.json()

    if settings.OWNER_FIELD not in identity_response:
        return None
    if identity_response[settings.OWNER_FIELD] == '':
        return None
    return identity_response


class IsAuth(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Write permissions are only allowed to the owner of the snippet.


        user = get_owner(request)
        if not user:
            return False

        return True



class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening