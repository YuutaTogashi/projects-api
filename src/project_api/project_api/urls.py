"""project_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from projects.views import ProjectList, ProjectDetail, MemberDetail, MemberList, SearchMember

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/projects/', ProjectList.as_view()),
    path('api/v1/members/search/', SearchMember.as_view()),
    re_path('api/v1/projects/(?P<project_hash>\w+)/$', ProjectDetail.as_view()),
    re_path('api/v1/projects/(?P<project_hash>\w+)/members/', MemberList.as_view()),
    re_path('api/v1/members/(?P<member_hash>\w+)/$', MemberDetail.as_view()),
]
